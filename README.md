# Welcome to UTD Computer Security Group Wiki

## How to contribute:
*We require the following*
  * All pages are written in markdown
  * Markdown must contain these components in the header:
    - Author
    - Creation Date
    - Last Edited Date
    - Topic Subject
  * **Keep the memeing to a minimum the occasion reference is okay but this is strictly an informational Wiki**

## How to Submit:
  1. Email Method:
    - Email *utdcsg@gmail.com*
    - The subject of the email must contain the word wiki with the subject of the Wiki
    - Example = `Wiki: Subject of wiki`
    - Attach the markdown file you want to submit
    - We will review the wiki and ask for any changes (if any are needed)
    - Once it is on the wiki we will email you the respective link
  2. Git Method:
    - Fork the [CSG Wiki](https://gitlab.com/utdcsg/wiki "CSG Wiki") on gitlab
    - Add Markdown file to subject folder or create a new folder if the topic does not match any given folder
    - Commit and Push to fork repo
    - Perform a Pull Request on Main repo on gitlab

## How to change a wiki:
  1. Email Method:
    - Use the same format as submitting a new wiki but add edit to the subject title
    - Example: Wiki[Edit]: Subject Title
    - Add a description on what is being changed, why, and sources for the change

  2. Git Method:
    - Fork the [CSG Wiki](https://gitlab.com/utdcsg/wiki "CSG Wiki") on gitlab
    - Edit the Markdown file
    - Commit and Push to fork repo
    - Perform a Pull Request on Main repo on gitlab

**If you have any questions email at utdcsg@gmail.com or contact us on slack #CSG**

## List of Topics that need to be created
  1. Linux Commands
    + Grep, ls, cat, sort, find
    + User Permssions, chmod, File Access Control
    + Git with SSH
    + Scripting in Linux
      + c/c++, bash, sh, python
  2. Linux Security
  3. Windows Commands
  4. Windows Security
  5. Networking and Infrastructure
  6. Penetration Testing
    - Steps of Penetration Testing
      - Recon
      - Enumeration
      - Exploitation
      - Privelage Esclation
      - Persistence
      - Cleanup
    - Masking Traffic
  7. Malware Analysis
  8. Reverse Engineering
