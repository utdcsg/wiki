# SSH Basics
`Created By: Andrew Mirghassemi`<br />
`Creation Date: 2018-02-16`<br />
`Last Edited: 2018-02-16`<br />
`Topic: SSH Basics`

*SSH Stands for Secure Shell and is mainly used to connect to another computer over a network to run commands on that computer.*<br /><br />
**This tutorial assumes you have openssh-client and openssh-server already installed and using systemctl as your service manager** <br /><br />
**This tutorial assumes you have basic knowledge of sudo**
## SSH Configuration
*Edit configuration in /etc/ssh/sshd_config must be superuser*
  1. Disabling root login: `PermitRootLogin no`
  2. Use Key authentication: `PubkeyAuthentication yes`
  3. Setting Password Authentication: `PasswordAuthentication yes` set to no if you want to use PubkeyAuthentication only
  4. Disabling the use of empty passwords: `PermitEmptyPassword no`


## Running SSH

  1. Starting the service
    - `sudo systemctl start ssh` this will start the service on your linux machine
    - `sudo systemctl status ssh` checks the status of ssh if it is running or not, this is also where you can see if you have any errors when running the service
    - `sudo systemctl enable ssh` makes a symobolic link between ssh.service file with the startup service to enable ssh to start on boot
    - `sudo systemctl restart ssh` restarts the service, this is required for every change you make to configuration file
    - Some linux distrubution default ssh service file is called sshd for secure shell daemon, if this is the case for your distrubution replace `ssh` with `sshd` in the commands above. This is the case if the above commands return with an error saying that `ssh.service does not exist`

## Generating SSH Keys

  1. Open terminal
  2. `ssh-keygen` This will use default options to generate an ssh key and ask for the location of the public key and private key as well as if you want to use a password with your key
     * By Default: it uses RSA as the type of encryption for your key
  3. `ssh-keygen -t ed25519` this will generate the ssh key and use the encryption method of ed25519. the "-t" option will allow you to choose the type of encryption.
  4. After creating the key you will see two files, `key.pub and key` These two files are very different and you must understand they key differences
    - The file ending with `.pub` is your public key; This is the key that you give to the server you want to connect to or paste into github/gitlab to identify with the pair
    - The file not ending with any extension is a private key. **DO NOT SHARE THIS** this key is private and i used to matched with the public key. you will use this key to login to the computer over ssh or push code to your git repo.
    - The private key file should be set with strict permissions on your computer incase of system compromises, `chmod 400 privatekey` will change the permssions of the file to be more strict. *for more information on chmod see another wiki (basically its similar to file access control)*
  5. After creating and understand the two different files you need to add the privatekey to your key agent to use the key properly
    - `ssh-agent` will start the ssh-keyagent to load keys into
    - `ssh-add privatekeyfile` will add priavtekeyfile to the key agent so you can use it to login with ssh or push to git repos for example

## Adding public key to server

  1. Go to folder .ssh in user home folder
  2. If this directory does not exist create it with `mkdir .ssh`, then use `chmod 700 .ssh` to set permissions
  3. create a file called authorized_keys
  4. copy public key information into authorized_keys then save files
  5. set permissions of authorized_keys with `chmod 600 authorized_keys`
  6. Restart ssh service `sudo systemctl restart ssh`

## Using ssh to login to servers

  1. Find ip address of server you want to connect to and make sure that the *sshd_config* and *authorized_keys* files are set correctly
  2. Run the service as instructed above on the server then go to client machine
  3. `ssh username@ipaddress` will connect to ssh with the user:"username" and connect to the machine with the ipadress:"ipaddress"
  4. `ssh -i privatekey user@ipaddress` will connect to the server with the same options above but identify the private key you want to use if you are using private key authentication. `-i` stands for identification and `--identify` can be use as well
  5. `ssh -p #### user@ipaddress` -p changes the default port being used if you change this option in the servers sshd_config <br />
  *all these options can be combined*
  6. Example of combined command: `ssh -i privatekey -p 1337 user@ipaddress` this will use the privatekey "privatekey" with port "1337" under username "user" on the machine "ipaddress"
