# Basic Linux Commands
`Created By: Andrew Mirghassemi`<br />
`Creation Date: 2018-02-16`<br />
`Last Edited: 2018-02-16`<br />
`Topic: Linux Basics`

   - Commands that will be covered in this wiki
     - Grep
     - ls
     - cat
     - sort
     - find
   - To view the man page of any linux command type: `man command`
     - Example: `man grep`
   - To view help page of any linux command type: `command --help`
     - Example: `grep --help`
   - Most linux commands are formatted as `commmand --option parameter`
   - This wiki is just to show the basics for more indepth coverage read the man page with full description


## Grep
  *Grep can be used to search for regular expressions inside of files, folders or even standard out. This tool is very useful when needing to find information within large amounts of data*

  1. Basics of grep:
    - `grep --option "pattern" input_file`
  2. Searching a single file for a string
    - `grep -e "string" file`
    - Example: `grep -e "generic" textfile.txt`
  3. Searching all files in a folder for a string
    - `grep -e "string" *`
    - "*" is used in linux as a wildcard
    - Example: `grep -e "generic" *`
  4. Searching binary files
    - If the file is a binary file then you might be getting `Binary File (standard input) matches` instead of the results
    - Change the option to -a or --text
    - Example: `grep -a "String" file`
  5. Search files and display lines before or after the search String
    - List # Lines before string + string:`grep -B# "string" file`
    - List # Lines after string + string: `grep -A# "string" file`
    - Example of Combined: `grep -B5 -A5 "string" file #This will show 5 lines before and after as well as the line the string is on`
  6. Piping standard output to Grep
    - command | grep option "string"
    - Example: `cat file | grep -a "string"`

## ls
  *ls is short for list in that when executing this command you are listing a directory and viewing the properites of those files*
  1. Basics of ls:
    - `ls` list the directory you are currently in
    - `ls path/to/diretory` list a directory you provide the path of
    - `ls /path/to/directory/from/root` ls also can use static paths so you can give it a final path
  2. Listing with more options:
    - `ls -l`
      - will list the current directory in vertical list instead of horizontal
      - will provide more information on file or folder listed such as `permssion owner creator size(in bytes) time last accessed`
    - `ls -a`
      - will list directory with hidden files
        - Hidden files usally start with "." Example: `.bash_history`
  3. Combining ls options
    - `ls -la` provides a list in vertical form with more information of hidden and nonhidden files

## cat
  *cat is uesed to concatenate files and print to the standard output*

  1. Basics of cat:
    - `cat filename` this will concatenate the file out to standard out
    - `cat /path/to/file` this will concatenate the file with a provided paths
  2. Outputing standard out to files:
    - `cat file > newfile` this will put the contents of "file" to "newfile"
  3. Using more or less with create
    - `cat file | more` will cat the file out with the more option
    - `cat file | less` will cat the file out with the less option
  4. Displaying line numbers
    - `cat -n file`

## sort
  *sort will take data and sort it based on a given parameter*

  1. Basics of sort:
    - `sort input_file` will sort the data in file in alphebetcal order
    - `sort input_file > output_file` will output the sorted data to another file
    - `sort -o output_file input_file` the -o option will output the sort to a given file
  2. Reversing the sort option
    - `sort -r input_file` well reverse the alphabetical order
    - This can be combined with other options to output the file, Example: `sort -ro output_file input_file`
  3. Ignoring Cases
    - `sort --ignore-case input_file`
    - This can also be combined with other options just like other linux commands
    - Example: `sort --ignore-case -r input_file`
  4. Checking if a file has been sorted or not
    - `sort -c input_file`

## find
  *allows you to find files and folder in your linux install*
  1. Basics of find:
    - `find -name filename` will search for files named "filename" in your current directory (this will recursively go inside directories)
    - `find -name "file*"` will search with a wildcard at the end of the string "file"
    - `find -iname filename` will search for a file but ignore the case
  2. Searching in a certain directory
    - `find /path/to/directory -name filename`
  3. Search for directories by name
      - `find / -type d -name directoryName` this will search the entire linux install for a directory, "-type" is used to define what type of file or folder you are searching for
  4. Searching for specific file types
    - `find -type f file.extension` this will search for a file with a certain extension
    - wildcards can be used in this case as well for example to search for all text files, `find -type f *.txt`
